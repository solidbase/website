---
title: "Imprint"
description: "legal issues"
date: 2020-02-18
---

<script>
_paq.push(['forgetConsentGiven']);
</script>

# Responsible for the content

With *content* the domain solidbase.info with the subdomains app.solidbase.info and learn.solidbase.info are named here. Responsible for content of subdomains of ld.solidbase.info is the respective user.

Solidarische Landwirtschaft e.V.  
Vorstand: Mathias von Mirbach, Lisa Haubner, Marianne Ohlhoff, Veikko Heintz, Claudia Höps  
Walburger Str. 2  
37213 Witzenhausen  

Steuernummer: 162 142 09938  
Gemeinnütziger Verein; Registergericht Kassel: VR 4941  

[solidbase[ätt]solidarische-landwirtschaft.org](solidbase[ätt]solidarische-landwirtschaft.org)


# Privacy Policy

## Solid

The [SolidBase App](https://app.solidbase.info) uses [Solid](https://solid.mit.edu) to store user data. Therefore the responsibility for the data is with the [Solid server providers](https://solid.inrupt.com/get-a-solid-pod).  

### inrupt.net

[Inrupt](https://inrupt.com) runs a *productive* instance of the solid server prototype on https://inrupt.net. They have well elaborated [Terms of Service](https://inrupt.com/terms-of-service).

### ld.solidbase.info

For development and experimental use purposes we have setup a [node-solid-server](https://github.com/solid/node-solid-server) instance on https://ld.solidbase.info. It is run by [ecobytes](https://ecobytes.net) without any guarantees for long term support, in particular but  among other things  not for data stability, integrity or general persistence.

Ecobytes, as in the role of service provider, can not overtake any responsibility for any user uploaded or generated content on ld.solidbase.info.

#### What & why

When you create a Solid Pod, Ecobytes stores the following information about you in order to provide the Service:

- Your email
- Your name
- Data stored on your POD

This information will only be used to reset your password upon your request, or to contact you in case of crucial service announcements. Your email address needs to be working to be able to reset your password.

Your password is saved in hashed form. The hashed password is saved so that your identity can be verified when logging in to your Solid Pod.

Your information will not be shared outside of Ecobytes and legally contracted Ecobytes service providers.

Ecobytes does not use your information for providing targeted advertising.

Ecobytes will not provide access to your data for any purpose other than providing it to those who have been given read access to it by the Solid Pod owner(s).

#### Where

The instance runs from source in a LXD container in a [hetzner datacenter](https://www.hetzner.com/), somewhere in Bavaria.

#### Acceptable Use Policy

By using our Services you agree not to misuse the Services or assist or enable anyone else to do so. For example, you must not try to do any of the following in connection with the Services:


- breach or otherwise circumvent any security or authentication measures;
- interfere with or disrupt any user, host, or network, for example by sending a virus, overloading, flooding, spamming, or mail-bombing any part of the Services;
- access, search, or create accounts for the Services by any means other than our publicly supported interfaces (for example, "scraping" or creating accounts in bulk);
- send unsolicited communications, promotions or advertisements, or spam;
- send altered, deceptive or false source-identifying information, including "spoofing" or "phishing";
- promote or advertise products or services other than your own without appropriate authorization;
- abuse referrals or promotions to get more storage space than deserved;
- circumvent storage space limits;
- sell the Services unless specifically authorized to do so;
- harass or abuse ecobytes personnel or representatives or agents performing Services on behalf of ecobytes;
- violate the law in any way, including storing, publishing or sharing material that's fraudulent, defamatory, or misleading;
- violate the privacy or infringe the rights of others;
- use the Services to violate any law or regulation.

In the event that any user is monopolising an unfair proportion of available system or network resources, we reserve the right to limit that user’s activity.

At our sole discretion, ecobytes reserves the right to delete and close accounts that do not adhere to the solidbase.info Terms of Service, Privacy Policy, or Acceptable Use Policy.

Ecobytes welcomes your comments regarding the Terms of Service, Acceptable Use Policy, and Privacy Policy. If you have questions or concerns about the Services, you can contact us at [support[ädd]ecobytes.net](mailto://support[ädd]ecobytes.net).

Websites under the solidbase.info domain may contain links to websites that are  owned, operated or controlled by other stakeholder, and, therefore, Netzwerk Solidarische Landwirtschaft e.V. nor ecobytes e.V. are not responsible for the content or your use of such websites. Please read the Terms of Service of any other website that you access from the solidbase.info websites, as they may be different from the solidbase.info Terms of Service.

## Matomo

### Purpose of the processing

This website is using [Matomo](https://matomo.org/), an Open Source, self-hosted software for collecting anonymous usage statistics for this website.

The data is used to analyse the behaviour of the website visitors to identify potential pitfalls like not found pages, search engine indexing issues and to find out which contents are the most appreciated. Once the data (number of visitors reaching a not found pages, viewing only one page…) is processed, Matomo is generating reports for website owners to take action, for example changing the layout of the pages, publishing some fresh content… etc.

### Which data is processed?

Matomo is processing the following data:

- Cookies
- Anonymized IP-address by removing the last 2 bytes (so 198.51.0.0 instead of 198.51.100.54)
- Pseudo-anonymized Location of the user (generated from the anonymized IP-address)
- Date and time
- Title of the page being viewed
- URL of the page being viewed
- URL of the page that was viewed prior to the current page (if the page allows it)
- Screen resolution
- Time in local timezone
- Files that were clicked and downloaded
- Link clicks to an outside domain
- Pages generation time
- Country, region, city (low resolution based on IP-address
- Main Language of the browser
- User Agent of the browser


## Webfonts

This website is using Fonts from [Google Fonts](https://fonts.google.com/) which get fetched from Google Servers when loading this website. You can find more information about the privacy implications [in the FAQs](https://developers.google.com/fonts/faq?hl=en#what_does_using_the_google_fonts_api_mean_for_the_privacy_of_my_users).

## Recipient of the data

The personal data received through Matomo are sent to:

- us (the owners of solidarische-landwirtschaft.org and solidbase.info)
- *[Johannes Winter](mailto://solidbase[ätt]solidarische-landwirtschaft.org)* (host and maintainer of this website)
- *[ecobytes.net](mailto://info[ätt]ecobytes.net)* (host of the server)

## Details of transfers to third country and safeguards

Matomo and this website data is hosted in *Germany*. No data leaves the EU.

## Data subject’s rights

As Matomo is processing personal data on legitimate interests, you can exercise the following rights:

- **Right of access and data portability**: you can ask us at any time to access your data.
- **Right to erasure and rectification**: you can ask us at any time to delete all the data we are processing about you.
- **Right to object and restrict processing**: you can object to the tracking of your data by using the following opt-out feature or by enabling [DoNotTrack](https://www.eff.org/issues/do-not-track) in your browser:

<iframe
        style="border: 0; height: 200px; width: 600px;"
        src="https://piwik.allmende.io/index.php?module=CoreAdminHome&action=optOut&language=en&backgroundColor=&fontColor=&fontSize=&fontFamily=">
</iframe>

By visiting this page your consent for being tracked with matomo is forgotten.


## The right to lodge a complaint with a supervisory authority

If you think that the way we process your personal data with Matomo analytics is infringing the law, you have the right to lodge a complaint with a supervisory authority.

## License

This privacy policy is based on the privacy policy of [the Matomo project](https://matomo.org/privacy-policy/) and licensed under [Creative Commons](https://creativecommons.org/licenses/by-sa/4.0/), so you can modify it and use it yourself.

---
title: "About Solid Base"
date: 2017-05-05
description: "SolidBase is an Erasmus+ Strategic partnership for the development of a training programm for enhancing the financial sustainability of Solidarity Based Food Systems (SFS)"
---

SolidBase is an [Erasmus+ Strategic Partnership](https://ec.europa.eu/programmes/erasmus-plus/opportunities/strategic-partnerships-field-education-training-and-youth_en). Read some details on https://urgenci.net/solid-base/ and https://www.solidarische-landwirtschaft.org/das-netzwerk/projekte/aktuell/  

Join the discussion on https://discourse.solawi.allmende.io/c/SolidBase  

Find the code in https://lab.allmende.io/solidbase

The consortium consists out of the following organizations:

<hr>
<center>[![TVE logo](/logos/tudatosvasarlo-logo4.png)](http://tudatosvasarlo.hu)</center>
<hr>
<center>[![Ampi logo](/logos/logo-ampi.jpg)](http://asociaceampi.cz/)</center>
<hr>
<center>[![Agronauten logo](/logos/logo_agronauten.png)](http://www.agronauten.net/)</center>
<hr>
<center>[![urgenci logo](/logos/logo-urgenci.png)](https://urgenci.net/)</center>
<hr>
<center>[![solawi logo](/logos/logo_solawi.png)](https://www.solidarische-landwirtschaft.org)</center>
<hr>
<center>[![Erasmus+ logo](/logos/logosbeneficaireserasmusright_en.jpg)](https://ec.europa.eu/programmes/erasmus-plus/opportunities/strategic-partnerships-field-education-training-and-youth_en)</center>
<hr>
All content licensed as
<center>[![creative commons](/logos/cc.logo.svg)](https://creativecommons.org/)</center>

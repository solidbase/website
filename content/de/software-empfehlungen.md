---
title: "Software Empfehlungen für Solawis"
description: "Die Ergebnsisse der Solid Base Forschung im Software Bereich komprimiert zu einer kommentierten Liste"
date: 2020-02-25
---

# Software Empfehlungen für Solawis

Für das erasmus+-Projekt [Solid Base](https://solidbase.info) haben wir europaweit unter Solawis und Foodcoops eine Umfrage durchgeführt in der wir gefragt haben, welche Software von ihnen verwendet wird.   Kombiniert mit vorheriger und laufender Forschung im Umfeld der freien Software Bewegung [*Free and Libre Open Source Software* (FLOSS)](https://www.gnu.org/philosophy/floss-and-foss.en.html) können wir nun die folgenden Empfehlungen für speziell für Solawi entwickelte Tools als auch für Werkzeuge von allgemeinerer Anwendbarkeit geben.

Dieser Text ist ebenfalls frei lizenziert. Er findet sich als [Quelltext im allmende lab](https://lab.allmende.io/solidbase/website/blob/master/content/de/software-empfehlungen.md).


## Allzweck-Bürowerkzeuge
Wenn eine neue Solawi gegründet werden soll, wird keine spezielle Software benötigt. Installiere einfach die Allzweck-Bürosoftware [LibreOffice](https://www.libreoffice.org/) und den Offline-E-Mail-Client [Thunderbird](https://www.thunderbird.net). Wenn du nicht möchtest, dass Dritte durch Deine Fenster spionieren, benutze eine [Linux](https://en.wikipedia.org/wiki/Linux) Distribution, z.B. [Ubuntu](https://ubuntu.com/desktop), [Fedora](https://getfedora.org/en/workstation/), [Manjaro](https://manjaro.org/) oder schauen Dir mal an an, was es sonst noch auf [distrowatch](https://distrowatch.com) gibt. Richtet eventuell eine Mailingliste bei eurem bevorzugten [Librehoster](#librehoster), und schon kann es losgehen!  
In unserer Umfrage fanden wir keine speziellen Lösungen im produktiven Einsatz für die drei Gruppen der in Solawis anfallenden Aufgaben *Planung der Ernte, Planung der Lieferung und Budgetplanung*. Entweder werden diese mit den speziellen  Solawi Tools erfüllt oder sie werden mit Hilfe der verschiedenen Lösungen zur **Tabellenkalkulation** durchgeführt.

## Spezielle CSA Werkzeuge

Solawi hast seine Ursprünge im internationalen Raum unter dem Begriff *Community Supported Agriculture (CSA)*. Unter diesen Begriff fällt eine Vielzahl von unterschiedlichen Organisationsformen, die auch jeweils unterschiedliche technische Anforderungen an die Verwaltungssoftware haben. In diesem Text werde ich nur tools vorstellen die speziell für deutschsprachige Solawis entwickelt wurden sind, oder sich vergleichsweise leicht anpassen lassen. Für einen umfassenderen Überblick kann ein Blick auf den englischsprachigen [Originaltext](https://solidbase.info/recommendations/#dedicated-csa-tools) und die [Präsentation](https://slides.solidbase.info) geworfen werden.

### OpenOlitor

[OpenOlitor](http://openolitor.org) ist eine mehrsprachige web-App für die Verwaltung von CSAs. Sie bietet Funktionen  wie Mitgliederverwaltung, Lieferplanung, Zahlungsverfolgung und das Verfassen von Berichten. Zusätzlich bietet das Mitgliederportal den Mitgliedern Zugang zu Informationen über ihre Mitgliedschaft, und das Registrieren für Arbeitstage am Bauernhof.  
OpenOlitor wurde 2015 in Bern in der Schweiz gegründet; es wurde mitgegründet von dem Schweizer Bundesministerium für Landwirtschaft. Die Gesellschaft überwacht die Instandhaltung des Codes und der Dokumentation. Es gibt eine wachsende internationale Gemeinschaft, mit [Sunu](http://sunu.eu) in Deutschland, die weitere Funktionen hinzufügt hat, um die Software internationaler zu gestalten.  
In der ersten Phase konzentrieren sie sich auf die Umsetzung von Besonderheiten der deutschen Verhältnisse, vor allem die Fähigkeit, Geldbeiträge an die Solawi in individueller Höhe abzubilden und Lastschriftlisten zu erstellen, die mit europäischen Bankstandards kompatibel sind. Ein weiteres faszinierendes Merkmal ist die vollständige Anpassungsfähigkeit mit direktem Zugriff auf die Datenbank mittels SQL-Abfragen. Die Kodierung wird von der professionellen Schweizer Softwarefirma [Tegonal](https://www.tegonal.com/) in [Scala](https://www.scala-lang.org/) durchgeführt.

### Juntagrico

[Juntagrico (JA)](https://github.com/juntagricojuntagrico) ist eine Entwicklung der deutschsprachigen CSA Ortoloco, die eher mit dem Netzwerk [solawi.ch](https://www.solawi.ch) verbunden ist. Es ist ein gut gemachtes Webtool nicht nur für die Mitgliederverwaltung, was in der Schweiz immer auch die Planung für Einsätze auf dem Land beinhaltet, da dieses dort obligatorisch ist. Juntagrico löst auch vorbildlich die verteilte Mitgliederverwaltung (Mitglieder können selbstständig eigene Details wie Anschrift ändern) einschließlich der genossenschaftlichen Einlagen und die Lieferplanung.

### ACP-Admin

Eine sehr vollständige Lösung, die keinen Bedarf für ACPs aus dem Netzwerk [FRACP](https://www.fracp.ch) (Fédération Romande d'Agriculture Contractuelle de Proximité - französischsprachiger Verband der vertragsgebundenen Landwirtschaft der Nähe) offen lässt, ist die von einem Mitglied von [Rage de Vert](https://www.ragedevert.ch/) durchgeführte Entwicklung [ACPAdmin](https://acp-admin.ch/). Einige weitere ACPs setzen es ein, eine deutschsprachige ACP testet es.  
ACP-Admin scheint gut an Solawi Verhältnisse anpassbar zu sein und halt als (momentan) einziges tool eine automatische Berechnung der Ernteaufteilung integriert

### Drupal

Innerhalb des [Solawi-Netzwerkes](https://www.solidarische-landwirtschaft.org) entstanden einige maßgeschneiderte Lösungen auf der Basis von [drupal](https://www.drupal.org/), die sogar die administrativen Bedürfnisse eines 1800 Mitglieder starken Solawi erfüllen können. Leider sind diese Lösungen noch nicht öffentlich verfügbar. Einzelheiten können eventuell persönlich geklärt werden. Kontakt kann zum Beispiel über unser [discourse forum](https://discourse.solawi.allmende.io/c/SolidBase) aufgenommen werden.


## Spezielle Tools für foodcoops und Mitgliederläden

Diese Tools unterscheiden sich von den Solawi-Tools dadurch, dass sie eine Shopfront für die Vorbestellfunktionalität benötigen. Das bemerkenswerteste Werkzeug in dieser Kategorie ist das [OpenFoodNetwork (OFN)](https://openfoodnetwork.org/) mit australischen Wurzeln. Es hat eine große internationale [Gemeinschaft](https://community.openfoodnetwork.org/) und einen globale Lenkungsausschuss für seine Entwicklung.

In Europa ist das am längsten etablierte foodcoop-Tool [Foodsoft](https://github.com/foodcoops/foodsoft), das in Berlin gegründet wurde und nun hauptsächlich von Amsterdam aus gepflegt wird.

Der relativ neue [foodcoopshop](https://www.foodcoopshop.com/) aus Österreich hat ein frisches Aussehen und eine frische Ausstrahlung und zielt auf die Förderung von Einkaufsgruppen vor allem in ländlichen Gebieten ab.

Aus Belgien stammt [RePanier](https://repanier.be) [(demo)](https://demo.repanier.be/en/). Es nutzt das Django-Framework und hat somit Unterstützung für i18n.

Wenn Du ein Drupal-Sitebuilder bist, könntest Du dich sich für die [coopshop-Module von geeks4change](https://gitlab.com/geeks4change/modules) interessieren.

## Garten-/Landwirtschaftlicher Betriebsplaner

Der Bereich der Betriebsplaner für die Marktgärtnerei oder den landwirtschaftlichen Betrieb ist alt und in der konventionellen Landwirtschaft ist viel Geld im Umlauf. Die größten Akteure der Agrotechnologie haben sich zusammengeschlossen, um [365FarmNet](https://www.365farmnet.com) zu gründen. Es handelt sich um eine vollständige Lösung für die Verwaltung aller Aspekte der Landwirtschaft, die für kleine Betriebe kostenlos ist. Wenn Dir Datensouveränität egal ist, scheint dies der richtige Weg zu sein.  
Für Biobauern ist ein Werkzeug in den USA entstanden, das die Bedürfnisse der Bauern sehr gut zu erfüllen scheint: [Tend](https://www.tend.ag). Seine Anwendbarkeit im europäischen Kontext ist jedoch noch nicht gegeben, da das metrische System noch nicht unterstützt wird.  
Ein ganz ähnliches Tool für die Gemüseanbauplanung  für den deutschen Markt ist kürzlich als software-as-a-service (SAAS) erschienen: [gemuese-anbauplaner.de](https://gemuese-anbauplaner.de)

Zwei Offline-Tools auf MS-Windows-Basis für die Verwaltung der ersten Welle von Boxenlieferdiensten seit den 1980er Jahren sind noch immer von Bedeutung: Das [PC G&auml;rtner](https://www.pcgaertner.de) ist eine Komplettlösung von der Ernteplanung bis hin zum Packen und auslieferung der Gemüsekisten. Der [AboBote](http://www.abobote.de) verfügt über Lieferplanungsfunktionalität.

Nur ein einziges (was gut ist, da man sich darauf konzentrieren könnte) **FLOSS-Tool** scheint im Bereich der praktischen Organisation des landwirtschaftlichen Betriebs aktiv zu sein: [Farm OS]( https://farmos.org). Es handelt sich um eine webbasierte Anwendung für die Betriebsführung, Planung und Buchführung. Sie wird mit dem Ziel entwickelt, eine Standardplattform für die Sammlung und Verwaltung landwirtschaftlicher Daten bereitzustellen. Im Ursprungsland USA wird FLOSS in der Landwirtschaft durch Vernetzung schnell voran gebracht: [OpenTEAM - Offenes Technologie-Ökosystem für das landwirtschaftliche Management] (https://openteam.community/).

## Mitgliederverwaltung
In der Kategorie Vereinsverwaltung gab es keinen Bericht über ein FLOSS-Web-Tool, das in der Produktion eingesetzt wird. Obwohl [Galette](http://galette.eu/) gute Aussichten hat, ein weltweit anwendbares Werkzeug der Mitgliederverwaltung zu werden.

Ein Werkzeug, das in Deutschland von einigen Solawis verwendet wird, ist das Java-basierte Offline-Tool [JVerein](http://www.jverein.de).

Ein sehr gut ausgearbeitetes kostenloses, aber ohne offener Lizenz, Vereinsverwaltungswerkzeug für den deutschsprachigen Raum ist [Campai]( https://campai.com).

Geeks4Change entwickelte auch einige Drupal-Module für [Open Membership Management - OMM](https://gitlab.com/geeks4change/modules/omm).

## Buchhaltung & Warenwirtschaftssysteme

Die Welt der Buchhaltung und des Rechnungswesens ist einer der ersten Bereiche, die digitalisiert wurden. Hunderte von Lösungen für die kaufmännische Buchhaltung und die Ressourcenplanung von Unternehmen (Enterprise Ressource Planning (ERP) - *Warenwirtschaftssysteme* im deutschsprachigen Raum) konkurrieren auf dem Markt. Nur wenige FLOSS-Tools sind in diesem hochkapitalisierten Bereich entstanden.  
Ein sehr mächtiges Werkzeug aus dem [GNU](https://www.gnu.org/software/) Universum ist [GNUCash](https://www.gnucash.org/). Es verfolgt ein minimalistisches Konzept und verfügt über eine vollständige Funktionalität der doppelten Buchhaltung. Du solltest in den wesentlichen Konzepten der Buchhaltung geschult sein, bevor Du es benutzt.  
In Deutschland ist ein weiteres FLOSS-Tool recht populär, das dieselbe Plattform ([Jamaica](https://www.willuhn.de/products/jameica/)) wie das Vereinsmanagement-Tool [JVerein](http://www.jverein.de) verwendet: [Hibiskus](https://www.willuhn.de/products/hibiscus/).

Der Markführer für Warenwirtschaftssysteme speziell für die Biobranche ist [BioOffice](https://www.biooffice-kassensysteme.de/).

Es sind zahllose weitere Lösungen verfügbar. Sehr oft sind sie in hohem Maße an die lokalen Gegebenheiten angepasst. Eine Auswahl:

- [Bokio](https://www.bokio.co.uk)
- [Szlamlazz](https://www.szamlazz.hu)
- https://fiken.no
- https://www.stormware.cz/pohoda/start/
- https://pinus-buchhaltungssoftware.ch
- https://www.banana.ch
- https://www.cresus.ch
- https://www.ilohngehalt.de
- https://www.datev.de
- https://www.collmex.de/

Für eine umfassendere Unternehmensorganisation können auch ERPs benutzt werden. Die folgenden FLOSS-ERPs könnten von gutem Nutzen sein:

- https://www.dolibarr.org/ (Basis für [CakeACP](https://gitlab.com/zpartakov/cakeACP/))
- https://www.odoo.com/ (Basis für genossenschaftliche Supermärkte, supported by [Coop IT easy](http://coopiteasy.be/))
- https://erpnext.com/ (FLOSS ERP nächster Generation)

## Arbeitszeit

Eine einfache Smartphone-Anwendung zur Verfolgung der Zeit, die für bestimmte Arbeiten auf den Feldern benötigt wird, ist [BeetClock] (http://www.beetclock.com). Sie ermöglicht die direkte Integration in das [NOFA](https://nofa.org/) Enterprise Analysis Workbook, ein von [Richard Wiswall](https://www.richardwiswall.com/), dem Autor von *The Organic Farmer's Business Handbook*, entwickeltes System zur Aufzeichnung des gesamten landwirtschaftlichen Betriebs.  

Für die Planung und Aufzeichnung der Arbeit, die das Stammpersonal leistet, experimentieren einige CSA mit der Verwendung der proprietären Webanwendung [Toggl](https://toggl.com).

In Norwegen haben einige CSA gute Erfahrungen mit der Verwendung des Koordinierungsinstruments für Freiwillige [Rubic](https://rubic.no) aus der Welt der Sportvereine gemacht.

Aus den USA kommt das erfolgreiche kostenlose, aber leider nicht libre-Tool [volunteersignup](https://www.volunteersignup.org).

Hierzu gehören auch Terminfindungs- und Umfrage-Apps wie [dudle](https://dudle.inf.tu-dresden.de) und [framadate](https://framadate.org/).

## Kommunikation

Fast jede vorhandene digitale Kommunikation benötigt eine Art von Vermittler im Internet, um Verbindungen herzustellen. Dies geschieht mit Hilfe von Servern, die von Organisationen, die Hosters genannt werden, betrieben werden. Deshalb ist das Werkzeug hier noch enger mit dem sozialen Umfeld verbunden, das es am Laufen hält, als bei eigenständiger Software. Da unsere Organisationen auf Kommunikation basieren und alle Werkzeuge, die wir für die Kommunikation verwenden, die Art und Weise beeinflussen, wie wir kommunizieren, sollte die Frage, wie wir gerne digital kommunizieren, gründlich überlegt werden.  
Das Internet bietet großartige Möglichkeiten für eine dezentralisierte, föderierte Kommunikation. Einige bekannte Unternehmen wollen das Netz neu zentralisieren, um alle Informationen über den Bürger zu sammeln, die sie für die Erzielung von Gewinn und Macht erhalten können. Wenn wir unsere Datenhoheit behalten wollen, müssen wir mit Hostern zusammenarbeiten, die sich an die Schlüsselbegriffe der Vernetzung halten, die Dezentralisierung, Föderation und Transparenz sind. Transparenz drückt sich in der strikten Anwendung von FLOSS und der Möglichkeit aus, sich selbst einzubringen, mit den Hostern zusammenzuarbeiten. Dies kommt in dem „Libre“ innerhalb von Librehoster zum Ausdruck und ist gut vergleichbar mit der "freiwilligen" Mitarbeit in einer Solawi.

### E-Mail

E-Mail ist eines der ältesten verteilten Internet-Protokolle. Kommunikationen kann ziemlich gut in selbstkontrolliertem Raum halten werden, wenn mit einem [librehoster](#librehoster) für das Postfach zusammengearbeitet wird.  Leute, die tief in der aktuellen IT-Entwicklung stecken, benutzen normalerweise keine E-Mail mehr als bevorzugten Weg für die digitale Kommunikation, da es inzwischen abgestimmte Werkzeuge für verschiedene Arten der digitalen Kommunikation (Chat, Forum) gibt. Dennoch ist E-Mail immer noch unübertroffen in Bezug auf ihre Zugänglichkeit und Reichweite, sie ist für die meisten CSA der bevorzugte Weg für den Versand von Push-Benachrichtigungen.  

Laut der Umfrage scheint der einzige Offline-Client, der verwendet wird, das Libre-Software-Tool [Thunderbird] (https://www.thunderbird.net) von [The Mozilla Foundation] (https://www.mozilla.org/en-US/foundation/) zu sein. Der einzige genannte Online-Mail-Client ist [Roundcube](https://roundcube.net). Es wird empfohlen, [Thunderbird](https://www.thunderbird.net) als Client zu verwenden, da er viel schneller ist und einige zusätzliche Funktionen hat, die Online-Clients nicht bieten können, z.B. Archivierungsfunktionalität.

### Emaillisten und Rundbriefe

Neben den Googlegroups werden [Mailman](http://www.list.org/) und [Sympa](https://www.sympa.org) für Mailinglistenfunktionalität verwendet.  
Das Versenden von E-Mails an alle oder Teile der Mitglieder ist die Kernfunktionalität aller spezifischen Solawi-Tools.  

Mailchimp ist eine fortschrittliche All-in-One-Newsletter-Lösung und wird von [ACPAdmin](https://github.com/ragedevert/acp-admin) für den Versand der Wochenliste mit dem verteilten Gemüse integriert.

[Mailtrain](https://mailtrain.org/) scheint eine brauchbare, freie Newsletter-Lösung zu sein.

### Messaging

Die Zukunft des Messaging ist dezentralisiert. Zur Auswahl stehen:

- https://matrix.org : Ausgeklügeltes Protokoll, Team- und persönliche Gespräche, Brücken zu anderen Messenger-Inseln.
- https://rocket.chat : intuitives UX
- https://telegram.org/ : beliebt und scheinbar angemessen sicher
- https://signal.org : verwendet Standard-Mobilfunknummern als Identifikatoren, sehr sicher

### Foren

Unserer Umfrage zufolge werden für forumähnliche Funktionen Facebook- und Google-Gruppen von vielen Solawis genutzt. Andere machten gute Erfahrungen mit dem FLOSS [Discourse] (https://discourse.org/) und dem gut abgehangenen [phpBB](https://www.phpbb.com).  
Eine Solawi verwendet auch das proprietäre Tool [muut](https://muut.com).

### Soziale Medien

Facebook wird wegen dessen Praktiken der Datenerfassung, der konzerninternen Richtlinien und der willkürlichen Weiterleitung von Nachrichten heftig kritisiert. Siehe als Beispiel den [Facebook-Cambridge Analytica Datenskandal] (https://en.wikipedia.org/wiki/Facebook%E2%80%93Cambridge_Analytica_data_scandal). Dennoch nutzen große Teile der Bevölkerung die Plattform für die Protokollierung ihrer täglichen Aktivitäten. Daher wird es von vielen weiterhin als ein wertvolles Feld für die Mitgliederrekrutierung angesehen.  In Finnland gibt es sogar ein erfolgreiches Konzept namens [REKO](http://urgenci.net/reko-a-winning-concept-in-finland/), die sich ausschließlich auf Facebook verlässt.

[Twitter](https://twitter.com) ist heute eines der wichtigsten Werkzeuge, um Nachrichten von A nach B zu bringen. [Ein Twitter-CEO hat kürzlich angekündigt, dass er bereit sei, zu einer dezentralen OpenSource-Struktur überzugehen](https://twitter.com/jack/status/1204766078468911106), was im Universum der freien Software tatsächlich bereits existiert und als [Fediverse](https://en.wikipedia.org/wiki/Fediverse) bezeichnet wird, mit [Mastodon](https://joinmastodon.org/) als geeignetem Einstiegspunkt.

### Content-Management-Systeme (CMS)

Dies ist im Grunde die Benutzerschnittstelle zu einer Website. Alle CMS von Interesse sind jetzt freie Software. Neben [WordPress](https://wordpress.org), das von fast allen Hostern angeboten wird, sind weitere CMS im Einsatz:

- https://www.concrete5.org/
- https://hexo.io/
- https://gohugo.io/
- https://getgrav.org/

[Drupal](https://www.drupal.org/) ist ein sehr fortschrittliches CMS, das [von einigen deutschen CSAs erfolgreich eingesetzt wird](#spezielle-csa-werkzeuge).

### Umgebungen für die Zusammenarbeit: Clouds

Neben dem üblichen Gdrive und der Dropbox wird FLOSS [Nextcloud](https://nextcloud.com/) von mehr and mehr Solawis eingesetzt. Sie ermöglicht nicht nur den Austausch von Dateien (sehr bald auch mit End-to-End-Verschlüsselung), sondern auch die gemeinsame Nutzung von Kontakten, Kalendern, Aufgaben und Formularen. Sogar die gemeinsame Bearbeitung von Online-Dokumenten und Videoanrufe funktionieren auf gut eingerichteten Servern. Es kannsich bei einigen Beispielprovidern direkt auf der [nextcloud website](https://nextcloud.com/providers/) angmeldet werden, aber es ist auch ein Kernangebot der meisten Librehoster.

Ziemlich revolutionär für die Online-Zusammenarbeit war die Entwicklung von Etherpads. Neben den ererbten [etherpad-lite](https://github.com/ether/etherpad-lite) ([öffentliche Server](https://github.com/ether/etherpad-lite/wiki/Sites-that-run-Etherpad-Lite#ssl-enabled-secure-data-transfer)) gibt es nun auch fortgeschrittenere Editoren, wie [codiMD](https://github.com/codimd/server) und [cryptPad](https://cryptpad.fr/).  Beachte, dass auch öffentliche Tabellenkalkulationsfunktionen oft von großem Nutzen sind. Siehe https://ethercalc.net/ für das. Dies ist sehr nützlich als Fallback und wenn noch keine Next Cloud am laufen ist.

Einige andere Umgebungen verdienen vielleicht einen Blick:

- [System D](https://www.system-d.org/) ist ein sehr frisches, aber ehrgeiziges Projekt, das derzeit einen gemeinsamen Kalender, eine Aufgabenliste, ein Forum und eine *interaktive Karte* mit weiteren geplanten Modulen kombiniert
- [crabgrass](https://0xacab.org/riseuplabs/crabgrass) ist ein solides Wiki mit feinkörniger Zugangsrechtskontrolle aus dem aktivistischem Universum

### Formulare und Fragebögen für Umfragen

Umfragen sind eine gute Möglichkeit, um Feedback von  Solawi Mitgliedern einzuholen oder neue Mitglieder zu werben.  
Kommerzielle Internet-Tools wie [Google Forms](https://www.google.com/forms/about/), [SurveyMonkey](https://www.surveymonkey.com/), [KwikSurveys](https://kwiksurveys.com/) oder [cognito forms](https://www.cognitoforms.com) bieten eine einfache und kostenlose Möglichkeit, Online-Umfragen zu erstellen. Mit diesen proprietären Anwendungen gibst du jedoch die Kontrolle über die aus den Fragebögen gesammelten Daten ab.
Es stehen verschiedene Alternativen zur Verfügung. Die [KoBo Toolbox](https://www.kobotoolbox.org) bietet eine kostenlose Registrierung auf ihrer Homepage an, und einige andere interessante FLOSS-Lösungen, die ein Hosting erfordern, können ebenfalls genutzt werden. Dies sind [Drupal-Webformulare](https://www.drupal.org/project/webform), [Limesurvey](https://www.limesurvey.org) und [OhMyForm](http://ohmyform.com). Nextcloud beinhaltet auch ein einfaches [Formular Plugin](https://apps.nextcloud.com/apps/forms).  
URGENCI, das internationale CSA-Netzwerk, hat Limesurvey in der Vergangenheit für Fragebögen mehrerer europäischer Projekte eingesetzt. Aktuellen Umfragen finden sich unter: https://questionnaires.urgenci.net.

### Videokonferenzen

Videokonferenzen, vor allem mit vielen Menschen, sind für die Kommunikationstechnologie immer noch keine triviale Aufgabe. Skype, als eine Microsoft-eigene Technologie, wird [wegen seiner Datenerfassungspraktiken heftig kritisiert](https://www.computerworld.com/article/2474090/new-snowden-revelation-shows-skype-may-be-privacy-s-biggest-enemy.html). Als Reaktion darauf ist mittlerweile die proprietäre US-basierte Plattform [zoom](https://zoom.us/) weit verbreitet.  

Die  freie Software [jitsi](https://jitsi.org/) ist ungeschlagen für ihre einfache Handhabung und ihre hohe Verbindungsqualität. Aber es benötigt [einen gut eingerichteten, nicht überlasteten Server und eine gute Bandbreite](https://github.com/jitsi/jitsi-meet/wiki/Jitsi-Meet-Instances#ssl-enabled-secure-data-transfer) (besser kein WIFI) für ein angenehmes Erlebnis.

## Provider für online Kommunikation

### Librehoster

- https://lab.libreho.st/librehosters
- https://riseup.net/de/security/resources/radical-servers
- [Chatons collective of independent hosters](https://chatons.org/en/chatons-collective-independant-transparent-open-neutral-and-ethical-hosters-providing-floss-based)

### Auswahl von Librehostern
- https://weho.st
- https://www.webarchitects.coop/
- https://www.hostsharing.net/
- https://indie.host/
- https://disroot.org/
- https://www.datenkollektiv.net
- https://systemausfall.org/


### Konstenlos (Freemium)
- https://www.seznam.cz/
- https://protonmail.com/
- https://protonet.com/

### Kommerzielle Anbieter
- https://www.gandi.net
- https://www.manitu.de/
- https://www.df.eu/
- https://aruba.it
- https://www.easyspace.com/
- https://www.infomaniak.com
- https://www.hosteurope.de/en/
- https://www.webnode.cz
- https://www.letshost.ie/
- https://www.hetzner.de


### All-in-one Webseitensysteme
- https://www.blogger.com
- https://www.squarespace.com/
- https://www.wix.com/
- https://www.facebook.com/

---
title: "Landwirtschaft braucht festen Boden unter den Füßen "
noToc: "true"
subTitle: "Bildungsreihe zur Stärkung finanzieller Nachhaltigkeit von Solawis"
description: "Das Netzwerk Solidarische Landwirtschaft hat zwischen September 2017 und Februar 2020 unter Leitung des internationalen Netzwerkes für Community Supported Agriculture (CSA) URGENCI, zusammen mit anderen europäischen Organisationen ein 4-tägiges Erasmus+ finanziertes Bildungsprogramm zur Förderung der finanziellen Nachhaltigkeit von Solawis entwickelt."
date: 2020-03-05
place: "Witzenhausen"
author: "Netzwerk Solidarische Landwirtschaft e.V."
---

Als Grundlage diente eine Untersuchung von der landwirtschaftlichen Forschungsgesellschaft *Die Agronauten* und *URGENCI*.
Aus Online-Umfragen unter CSA Akteur*innen und Konsument*innen, zahlreichen Interviews und einer eingehenden Literaturrecherche in Europa und Nord-Amerika wurden zentrale Fragen entwickelt die in der Bildungsreihe thematisiert werden. Die Bildungsmodulen wurden in verschiedenen Ländern durch die Partnerorganisationen AMPI (Asociace místních potravinových iniciativ - Verband lokaler Lebensmittelinitiativen) aus Prag und TVE (Tudatos Vasarlo - Bewusste Käufer) aus Budapest erprobt. Alle Trainingsmaterialien sind mehrsprachig frei im Internet zugänglich. Ein zusammenfassendes Booklet (siehe link) ist auf der Projektseite verfügbar.

### Bildungsreihe
#### Modul I - Verwaltung

Wie erreichen Solawis organisatorische Stabilität? Welche Rechtsformen kommen in Frage? Wie können Mitglieder in Entscheidungsprozesse eingebunden werden? Anhand von Erfahrungswerten mit aktuellen Organisationsentwicklungsmodellen wie der Soziokratie werden im *Modul I* Antworten auf diese Fragen gegeben.

#### Modul II - Budgetplanung

Solawi ist eine echte Partnerschaft zwischen Konsument*innen und Produzent*innen in dem die Verbrauchergemeinschaft die Kosten des landwirtschaftlichen Betriebes verbindlich für ein Jahr trägt. So verringert sich der Marktdruck. Wie funktioniert die Budgetplanung? Welche Kosten sind dabei zu kalkulieren? Wie wird das Budget am besten an die Mitglieder kommuniziert? Diese Fragen werden in *Modul II* bearbeitet.

#### Modul III - Medienkompetenz

Für die Organisation von größeren Gruppen sind heute digitale Anwendungen unabdingbar. Welche Tools kommen dafür in Frage? Welche Anwendungen können die eigene Daten und Nutzungssouveränität fördern? In dem dritten Modul stellen wir für Solawis nutzbare Tools vor.

#### Modul IV - Mitgliedereinbindung

Eine Solawi ist eng an die jahreszeitlichen Rhythmen angebunden. Daran können sich kulturelle und soziale Prozesse orientieren. Das Konzept von storming-forming-norming-performing Phasen bei der Gruppenbildung ist eine Möglichkeit das Ziel gemeinsam zu entwickeln und zu erreichen. Das vierte Modul beschäftigt sich mit Mitgliedereinbindung und gruppendynamischen Konzepten, die helfen den Community-building Prozess zu unterstützen.

### App Entwicklung

Schon länger ist bekannt dass die Kommunikation der eigenen finanziellen Bedürfnisse und der Notwendigkeit von alternativen Wirtschaftspraxen wie Solawi (nicht nur) für Landwirt*innen eine komplexe Aufgabe darstellt.
Im Rahmen des Projekts wurde eine Web-App entwickelt die für vereinfachte  Kommunikation von Budgetdaten und deren Erläuterung an die Mitgliedschaft gedacht ist: https://app.solidbase.info


Projektseite: https://www.solidarische-landwirtschaft.org/das-netzwerk/projekte/solid-base/

![Netzwerk Solidarische Landwirtschaft e.V.](/logos/logo_solawi.png)

---
title: "Software recommendations"
description: "Here you find the results of the solidbase software research compressed to a commented list of software recommendations."
date: 2020-02-25
---
# SolidBase Software recommendations
For the erasmus+ project *solid base* we conducted a survey where we asked *Solidarity based Food Systems* (SFS) which software is in use by them. Combined with prior and ongoing research in the [*Free and Libre Open Source Software* (FLOSS)](https://www.gnu.org/philosophy/floss-and-foss.en.html) milieu, we can now give the following recommendations for dedicated CSA tools and for tools of more general applicability.

This text is also freely licensed. Find it's [source here](https://lab.allmende.io/solidbase/website/blob/master/content/recommendations.md).

## General purpose office tools
If you start a SFS you don't need any special software. Just install the general purpose office suite [LibreOffice](https://www.libreoffice.org/) and the offline email client [Thunderbird](https://www.thunderbird.net). If you don't want any third party to spy through your windows, use a [Linux](https://en.wikipedia.org/wiki/Linux) distribution, e.g. [Ubuntu](https://ubuntu.com/desktop), [Fedora](https://getfedora.org/en/workstation/), [Manjaro](https://manjaro.org/) or checkout what else is out there on [distrowatch](https://distrowatch.com). Maybe arrange a mailinglist with your preferred [Librehoster](#providers-for-online-communication), and your ready to go!  
In our survey we found no special solutions in productive use for the the three groups of SFS activity **crop planning, delivery planning and budget planning**. Either these are fulfilled with the dedicated CSA tools or they are done with **spreadsheets**.

## Dedicated CSA tools
In europe several flavors of CSA emerged from the various regions. In France various solutions are competing for the users attention. The most notable are:

- [AMAPJ](http://amapj.fr/) is a development from an AMAP of the Drome written in Java. Well done documentation is available on the website.
- [Cagette.net](https://www.cagette.net/) had been developed by the *agency for training producers in digital skills and communication for food short circuit projects [Alilo](https://www.alilo.fr/)*. It is written in the language from gaming industry [HAXE](https://haxe.org/) which produces a very clean GUI. As only one instance is used for all groups, it can offer a good mapping functionality out of the box. On the website you can find a tour through the application.
- [AMAPress](http://amapress.fr/) is a WordPress plugin for enabling the easy organisation of AMAPs
- [Clic'AMAP](https://clicamap.amap-aura.org/) which license status is unclear originates from the region Auvergne-Rhône-Alpes and [is used now in 2020 throughout France](https://amap-aura.org/outil-de-gestion-amapk/).

Interestingly all other dedicated CSA tools have origins in Switzerland. But [sunu](http://www.sunu.eu) [(demo)](https://sunuwwwtest.applicationcloud.io), which is coordinated by an association spread over Germany, is working on the adaptation of the Swiss development [OpenOlitor](http://www.openolitor.ch) [(demo)](https://wwwtest.openolitor.ch) to global applicability. In the first phase they concentrate on implementing features special to German circumstances, i.e. the ability to map monetary contributions to the Solawi of individual height and the ability to generate direct debit lists compatible with European bank standards. Another intriguing feature is the complete adaptability with direct access to the database using SQL queries. The coding is done by the professional Swiss software company [Tegonal](https://www.tegonal.com/) in [Scala](https://www.scala-lang.org/).

A very complete solution which leave no need for ACPs from the [FRACP](https://www.fracp.ch) network (Fédération Romande d’Agriculture Contractuelle de Proximité - French-speaking Federation of Contractual Agriculture of Proximity) unfullfilled is the development done by a member of [Rage de Vert](https://www.ragedevert.ch/) is [ACPAdmin](https://acp-admin.ch/). Some more ACPs are now using it, one german speaking ACP is testing it.  
This tool is (currently) the only tool that implements an automatic calculation of the crop allocation.

[Juntagrico (JA)](https://github.com/juntagricojuntagrico) is a development by the german speaking CSA Ortoloco, more connected to the [solawi.ch](https://www.solawi.ch) network. It is quite a neat webtool for compulsory co-worker management, distributed members administration including their cooperative deposits and delivery planning.

Another interesting development out of FRACP is [CakeACP](https://gitlab.com/zpartakov/cakeACP/). It is build on top of the ERP [dolibarr](https://www.dolibarr.org/). This is an interesting concept as it allows for a quite complete administration of the ACP.

Within the [Solawi network](https://www.solidarische-landwirtschaft.org) some custom tailored solutions based on [drupal](https://www.drupal.org/) emerged that can fulfill even the administrative needs of a 1800 member Solawi. Alas these solutions are not yet publicly availbale. For details get into contact. For example using our [discourse forum](https://discourse.solawi.allmende.io/c/SolidBase).

## Dedicated foodcoop tools

These tools differ from the CSA tools in the need of having a shopfront for offering pre-ordering functionality. The most remarkable tool in this category is the [OpenFoodNetwork (OFN)](https://openfoodnetwork.org/) with Australian roots. It has a large international [community](https://community.openfoodnetwork.org/) and a global steering committee for it's development.

In Europe, the longest established foodcoop tool is [Foodsoft](https://github.com/foodcoops/foodsoft) founded in Berlin and now maintained mainly from Amsterdam.

The relatively new [foodcoopshop](https://www.foodcoopshop.com/) from Austria has a fresh look and feel and aims at fostering buying groups especially in rural areas.

From Belgium originates [RePanier](https://repanier.be) [(demo)](https://demo.repanier.be/en/). It uses the Django framework and has thus support for i18n.

If you are a drupal sitebuilder you might be interested in the [coopshop modules from geeks4change](https://gitlab.com/geeks4change/modules).


### Common functionality of SFS tools

On the following table the common functionalities these SFS tools share. Of course all the functionalities can also be accomplished by smaller tools with a more general applicability.

Functionality         | Juntagrico | Sunu | Cagette | OpenFoodNetwork | ACP-Admin| AMAPJ
:---------------------|:--:|:----:|:-------:|:---:|:--------:|:----:
Members administration | x  |   x  |    x    |  x  |     x    |  x
Email members         | x  |   x  |    x    |  x  |     x    |  x
Delivery planning     | x  |   x  |    x    |  x  |     x    |  x
Share management      | x  |   x  |    x    |     |     x    |  x
Variable Orders       |    |      |    x    |  x  |          |  x
Control of payments   |  x |   x  |    x    |  x  |     x    |  x
SEPA generation       |    |   x  |         |     |          |
Volunteer Management  |  x |   x  |         |     |    x     |  

<br>

See details and screenshots of these specialized tools on https://slides.solidbase.info/


## Garden/farm planners

The field of market gardener / farm management tools is old and a lot of money circulates in the conventional farming industry. The biggest players in agrotechnology united to create [365FarmNet](https://www.365farmnet.com). It is a complete solution for managing all aspects of farming, free for small farms. If you don't care about data souvereignty this is the way to go.
For organic farmers a tool from the US emerged that seems to fullfill the needs of the farmers very well: [Tend](https://www.tend.ag). It's applicability in european context is although not given yet, due to non existant support for the metric system.

Two offline MS-Windows based tools for the management of the first wave of box delivery services starting in the 1980's are still of notability: The [PC G&auml;rtner](https://www.pcgaertner.de) is a all in one solution from crop planning to door delivery Box schemes. The [AboBote](http://www.abobote.de) only has delivery planning functionality but in a well elaborated fashion.

Only one (which is good, as concentration could be put onto it) **FLOSS tool** seems to be active in the field of farm management: [Farm OS]( https://farmos.org). It is a web-based application for farm management, planning, and record keeping. It is being developed with the aim of providing a standard platform for agricultural data collection and management. In the originating country US, FLOSS usage in agriculture is going fast through networking: [OpenTEAM - Open Technology Ecosystem for Agricultural Management](https://openteam.community/).

## Members administration
In the association management category there had been no report about any FLOSS web tool that is used in production. Although [Galette](http://galette.eu/) has good prospects to become a membership administrative tool of global applicability. Another similar development could be of interest for French SFS: [Garradin](https://garradin.eu/).

One tool that is used in Germany by some Solawis is the Java based offline tool [JVerein](http://www.jverein.de).

In Finland a members adminstrative tool became common, called [Flo members](https://flomembers.fi).

A very well elaborated free but not libre association management tool for German speaking areas is [Campai]( https://campai.com).

The Geeks4Change also developed some drupal modules for [Open Membership Management - OMM](https://gitlab.com/geeks4change/modules/omm).


## Bookkeeping

The world of bookkeeping and accounting is one of the first realms that became digitalised. Hundreds of commercial bookkeeping and enterprise ressource planning solutions are competing at the market. Only few FLOSS tools have emerged in this highly capitalized field. One very powerful tool, fully internationalized,  from the [GNU](https://www.gnu.org/software/) universe is [GNUCash](https://www.gnucash.org/). It sticks to the basics and has complete double-entry accounting functionality. You should be trained in essential concepts of accounting before using it.  
In Germany another FLOSS tool is quite popular that uses the same platform ([Jamaica](https://www.willuhn.de/products/jameica/)) as the association management tool [JVerein](http://www.jverein.de): [Hibiscus](https://www.willuhn.de/products/hibiscus/).

Notable free online tools are [Bokio](https://www.bokio.co.uk) from the UK and [Szlamlazz](https://www.szamlazz.hu) from hungary.

Copious commercial solutions are available. Very often they are highly adapted to local circumstances. A selection:

- https://fiken.no
- https://www.stormware.cz/pohoda/start/
- https://pinus-buchhaltungssoftware.ch
- https://www.banana.ch
- https://www.cresus.ch
- https://www.ilohngehalt.de
- https://www.datev.de
- https://www.collmex.de/

For more complete business organisation you might want to use an enterprise ressource planning (ERP) tool. The following FLOSS ERPs might be of good use:

- https://www.dolibarr.org/ (Base of [CakeACP](https://gitlab.com/zpartakov/cakeACP/))
- https://www.odoo.com/ (Base of cooperative shopping systems by [Coop IT easy](http://coopiteasy.be/))
- https://erpnext.com/ (Next generation FLOSS ERP, already equipped with basic agricultural functionalities)

## Working time

A simple smartphone app to track the time needed to fulfill some work in the fields is [BeetClock](http://www.beetclock.com). It allows for direct integration into the [NOFA](https://nofa.org/) Enterprise Analysis Workbook,a whole-farm recordkeeping system developed by [Richard Wiswall](https://www.richardwiswall.com/), author of *The Organic Farmer's Business Handbook*.  

For planning and recording the labor the regular staff is doing, some SFS are experimenting with using the proprietary web app [Toggl](https://toggl.com).

In Norway some CSA made good experience with using the volunteer coordinating tool [Rubic](https://rubic.no) out of the sports club world.

From the US is coming the successful free but alas not libre tool [volunteersignup](https://www.volunteersignup.org).

Here also belong date finding and poll apps like [dudle](https://dudle.inf.tu-dresden.de) and [framadate](https://framadate.org/).

## Communication

Almost all digital communications available needs some kind of intermediary in the internet to establish connections. This is done using *servers* that are run by organizations called *hosters*. That's why here the tool is even more closely related to the social environment that keps it running than in standalone software. As our organisations are based on communication, and all tools we use for communication influence the way we communicate, the question on how we like to communicate digitally should be thoroughly deliberated.  
The internet offers great possibilities for decentralized, federated communication. Some well known companies like to recentralize the net to gather all information about the citizen they can get for gaining profit and power. If we want to keep our data sovereignty we need to cooperate with hosters that stick to the key concepts of networking, that are decentralization, federation and transparency. Transparency is expressed by the strict use of FLOSS and the possibility to involve oneselve, to collaborate with the hosters. This is expressed by the *libre* within [librehoster](#lists-of-libre-hosters) and is well comparable to "volunteer" co-work on a CSA.

### Email
Email is the one of the oldest internet distributed protocols. You can keep your communications quite well in self-controlled space if you cooperate with a [librehoster](#lists-of-libre-hosters) for your inbox.  People that are deep into current IT development usually don't use email as their preferred way for digital communication anymore, as aligned tools for different kinds of digital communication (chat, forum) now exists. Nonetheless email is still unbeaten for it's accessibility and outreach, it is the preferred way for sending push notifications for most SFS.  

According to the survey, the only offline client in use seems to be the libre software tool [Thunderbird](https://www.thunderbird.net) from [The Mozilla Foundation](https://www.mozilla.org/en-US/foundation/). The only named online mail client is [Roundcube](https://roundcube.net). It is recommended to use [Thunderbird](https://www.thunderbird.net) as client as it is much faster and has some additional features onlineclients can't offer, i.e. archiving functionality.


### Emaillists & newsletters

Next to googlegroups [Mailman](http://www.list.org/) and [Sympa](https://www.sympa.org) are used for mailinglist functionality.  
Sending emails to all or parts of the membership is core funtionality of all dedicated SFS tools.  

Mailchimp is an advanced all-in-one newsletter solution and is integrated by [ACPAdmin](https://github.com/ragedevert/acp-admin) for sending the weekling list of distributed vegetables.

[Mailtrain](https://mailtrain.org/) seems to be a usable free newsletter solution.


### Messaging

The future of messaging is decentralized. You can choose from

- https://matrix.org : most sophisticated protocol, team and personal chatting, bridges to other messenger bubbles
- https://rocket.chat : intuitive UX
- https://telegram.org/ : popular and apparently reasonable secure
- https://signal.org : uses standard mobile numbers as identifiers, very secure

### Forums

According to our survey, for forum-like functionality facebook and google groups are used by a lot of SFS. Others made good experience with the FLOSS [Discourse](https://discourse.org/) and the well hung [phpBB](https://www.phpbb.com).
One Solawi ist also using the proprietary tool [muut](https://muut.com).

### Social media

Facebook is heavily criticized for it's data gathering, policy making and arbitrary message routing practices. See  [Facebook–Cambridge Analytica data scandal](https://en.wikipedia.org/wiki/Facebook%E2%80%93Cambridge_Analytica_data_scandal) as an example. Nonetheless large parts of the population use it for logging their everyday activities. Hence it is seen as a valuable field for member recruitment for copious SFS. There does even exist a successful buying group concept in Finland called [REKO](http://urgenci.net/reko-a-winning-concept-in-finland/) that relies entirely on Facebook.

[Twitter](https://twitter.com) is now one of the most important tools to get news from A to B. [A twitter CEO lately announced to be willing to move to a OpenSource decentralized structure](https://twitter.com/jack/status/1204766078468911106), what actually already exists in the the free software universe and is called the [Fediverse](https://en.wikipedia.org/wiki/Fediverse) with [Mastodon](https://joinmastodon.org/) as a suitable entry point.

### Content Management Systems (CMS)

This is basically the user interface to a website. All CMS of interest are libre software now. Next to [WordPress](https://wordpress.org) which is offered by almost all hosters, other CMS in use are:

- https://www.concrete5.org/
- https://hexo.io/
- https://gohugo.io/
- https://getgrav.org/

[Drupal](https://www.drupal.org/) is a very advanced CMS which is [successfully used by some German CSAs](#dedicated-csa-tools).

### Environments for collaboration: clouds

Next to the conventional gdrive and dropbox the FLOSS [Nextcloud](https://nextcloud.com/) is used by more and more CSAs. It not only enables filesharing (very soon also with end-to-end rncryption) but also sharing of contacts, calendars, tasks and forms. Even online collaborative document editing and video calls work on well set up servers. You can sign up to some example providers directly on the [nextcloud website](https://nextcloud.com/providers/) but it is also a core offer of most libre hosters.

Quite revolutionary for online collaborations was the development of etherpads. Next to the herited [etherpad-lite](https://github.com/ether/etherpad-lite) ([public servers](https://github.com/ether/etherpad-lite/wiki/Sites-that-run-Etherpad-Lite#ssl-enabled-secure-data-transfer)) more advanced editors now also exist, like [codiMD](https://github.com/codimd/server) and [cryptPad](https://cryptpad.fr/).  Note, that also public spreadsheet functionality is often of great use. See https://ethercalc.net/ for that. This is very useful as a fallback and  when you don't have a nextcloud running currently.

Some other environments might deserve a look:

- [System D](https://www.system-d.org/) is a very fresh but ambitious project that currently combines a shared calendar, a tasks list, a forum and a *interactive map* with more modules planned
- [crabgrass](https://0xacab.org/riseuplabs/crabgrass) is a solid wiki with finegrained acces right control from the activist universe

### Forms and questionnaires for surveys

Surveys are a great way to invite feedback from SFS members or for recruiting new members.  
Commercial internet tools like [Google Forms](https://www.google.com/forms/about/), [SurveyMonkey](https://www.surveymonkey.com/), [KwikSurveys](https://kwiksurveys.com/) or [cognito forms](https://www.cognitoforms.com) offer an easy and free way to create online surveys. However, with these proprietary apps, you don’t have any control over the data gathered from the questionnaires.
Various alternatives are available. [KoBo Toolbox](https://www.kobotoolbox.org) offers free registration on their homepage and some other interesting FLOSS solutions that require hosting are also possible to use. These are [Drupal webforms](https://www.drupal.org/project/webform), [Limesurvey](https://www.limesurvey.org) and [OhMyForm](http://ohmyform.com). There's also a simple [form plugin for nextcloud](https://apps.nextcloud.com/apps/forms).
URGENCI, the International CSA Network, has in the past deployed Limesurvey for questionnaires of several European projects. You can find the current surveys at: https://questionnaires.urgenci.net.

### Videoconferencing

Videoconferencing, especially with many people, is still no trivial task for communication technology. Skype, as a Microsoft owned technology, is [criticized heavily for it's data gathering practices](https://www.computerworld.com/article/2474090/new-snowden-revelation-shows-skype-may-be-privacy-s-biggest-enemy.html). As a response the proprietory US based platform [zoom](https://zoom.us/) is now used widely.  

The libre software [jitsi](https://jitsi.org/) is unbeaten for it's easiness of use and it's high quality connections. But you need [a well set up, not overloaded server and good bandwith](https://github.com/jitsi/jitsi-meet/wiki/Jitsi-Meet-Instances#ssl-enabled-secure-data-transfer) (better no WIFI) to make it a pleasurable experience.


## Providers for online communication

### Lists of libre hosters
- https://lab.libreho.st/librehosters
- https://riseup.net/de/security/resources/radical-servers
- [Chatons collective of independent hosters](https://chatons.org/en/chatons-collective-independant-transparent-open-neutral-and-ethical-hosters-providing-floss-based)

### Selection of libre hosters
- https://weho.st
- https://www.webarchitects.coop/
- https://www.hostsharing.net/
- https://indie.host/
- https://disroot.org/
- https://www.datenkollektiv.net
- https://systemausfall.org/


### Free
- https://www.seznam.cz/
- https://protonmail.com/
- https://protonet.com/

### Costing
- https://www.gandi.net
- https://www.manitu.de/
- https://www.df.eu/
- https://aruba.it
- https://www.easyspace.com/
- https://www.infomaniak.com
- https://www.hosteurope.de/en/
- https://www.webnode.cz
- https://www.letshost.ie/
- https://www.hetzner.de


### All in one website systems
- https://www.blogger.com
- https://www.squarespace.com/
- https://www.wix.com/
- https://www.facebook.com/

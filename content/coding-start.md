---
title: "Development of SolidBase budget planning tool started"
description: "For the educational program of the Erasmus+ program solidbase we create a budget presentation and planning tool. Read more about it's beginnings."
date: "2018-11-01"
---

After some time of settling of the technical foundations, we can now present the software stack to use for the development of the educational budget presentation and planning tool for solidarity based food systems, SolidBase.

As frontend framework we will use the advanced JavaScript based [Vue.js](https://vuejs.org/) framework. Using it's storage facility [Vuex](https://vuex.vuejs.org/), we will access [SoLiD PODs](https://solid.mit.edu/) with [RDF](https://en.wikipedia.org/wiki/Resource_Description_Framework) vocabulary from the [valueflows](https://www.valueflo.ws/) universe. Finding fitting words for the economic processes we want to present was quite hard, but with the decade long experience of the valueflows creators, it was apparently not that difficult for them to give us [this turtle file](https://lab.allmende.io/solidbase/valueflows/raw/plan/release-doc-in-process/budget.ttl).

For the first sketches of the user interface visit the [public instance](https://app.solidbase.info).

Development happens in the [allmende lab](https://lab.allmende.io/solidbase/solidbase).

Join the discussion in the [solawi discourse](https://discourse.solawi.allmende.io/c/SolidBase/io2).

Digital infrastructure is kindly powered by the ecological IT collective [ecobytes](https://ecobytes.net/).

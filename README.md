# `solidbase.info`

This is the source code for [`solidbase.info`](https://solidbase.info).

## Dependencies

- [Hugo](https://gohugo.io/getting-started/installing/)
- [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

## Run locally

```
git clone git@lab.allmende.io:solidbase/learn.git
cd learn
git submodule sync
git submodule update --init --recursive --remote
hugo serve
```

## Deployment

This repo uses [gitlab CI/CD](https://docs.gitlab.com/ce/ci/#exploring-gitlab-ci-cd) for deployment to [dokku](http://dokku.viewdocs.io/dokku/). See the [pipeline configuration](https://lab.allmende.io/solidbase/website/blob/master/.gitlab-ci.yml) for details.  
The following Variables needs to be set as under `Settings->CI/CD->Variables`:

- `APP_NAME_DEV`:  The SLD name for the *staging instance*, which will become accessible under `$APP_NAME_DEV.dokku.ecobytes.net`
- `APP_NAME`: A fqdn for the *production instance*
- `SSH_PRIVATE_KEY`: A key for the dokku, defined as project wide variable

Deployment to *production* needs to be maually triggered in the pipeline view.

## Authors

- Johannes Winter
- Jon Richter

## License

[GPL3](LICENSE)
